# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 14:35:49 2019

@author: IF
"""
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
corpus = [
    'This is the first document.',
    'This is the second document.',
    'And this is the third one document.',
    'Is this the first document?',
]
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)
print(vectorizer.get_feature_names())
print(X)
cVectorizer = CountVectorizer()
Y = cVectorizer.fit_transform(corpus)
print(Y.toarray())
print("Sum of arr(axis = 0) : ", np.sum(Y, axis = 1))